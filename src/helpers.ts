import { Nullable, QuestCondition } from "./interfaces";

export const mapConditions = (conditions: Nullable<string>): QuestCondition => {
  const [f, s] = conditions
    ?.split(',')
    ?.filter(n => n.length > 0)
    ?.map(n => parseInt(n)) ?? [];

  return [f, s];
};
