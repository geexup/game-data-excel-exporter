import {
  ExcelQuestData,
  ExcelQuestTarget,
  ExcelQuestReward,
  QuestResult,
  QuestTarget,
  QuestReward
} from "../interfaces/index";
import { makeReverseLinkedMap } from "../utils";
import { mapConditions } from "../helpers";

export class QuestMapper {
  excelTargetQuestMap = makeReverseLinkedMap(this.excelQuestTargetsData, 'quest_id');
  excelRewardQuestMap = makeReverseLinkedMap(this.excelQuestRewardsData, 'quest_id');

  constructor(
    private readonly excelQuestData: ExcelQuestData[],
    private readonly excelQuestTargetsData: ExcelQuestTarget[],
    private readonly excelQuestRewardsData: ExcelQuestReward[],
  ) {}

  public mapAll(): QuestResult[] {
    return this.excelQuestData.map((item) => this.mapOne(item));
  }

  public mapOne(excelQuest: ExcelQuestData): QuestResult {
    const excelQuestTargets = this.excelTargetQuestMap.get(excelQuest.id) ?? [];
    const excelQuestRewards = this.excelRewardQuestMap.get(excelQuest.id) ?? [];

    return {
      ID: excelQuest.id,
      Name: excelQuest.name,
      Cooldown: excelQuest.cooldown,
      Description: excelQuest.description,
      TimeLimit: excelQuest.timelimit,
      RequiredQuests: excelQuest.requiredQuests,
      Targets: excelQuestTargets.map((item) => this.mapQuestTarget(item)),
      Rewards: excelQuestRewards.map((item) => this.mapQuestReward(item)),
    };
  }

  private mapQuestTarget(excelQuestTarget: ExcelQuestTarget): QuestTarget {
    return {
      type: excelQuestTarget.type,
      description: excelQuestTarget.description,
      questType: excelQuestTarget.questType,
      extra: excelQuestTarget.extra ?? '',
      count: excelQuestTarget.count,
      conditions: mapConditions(excelQuestTarget.conditions),
    };
  }

  private mapQuestReward(excelQuestReward: ExcelQuestReward): QuestReward {
    return {
      type: excelQuestReward.type,
      description: excelQuestReward.description,
      extra: excelQuestReward.extra ?? '',
      count: excelQuestReward.count,
      conditions: mapConditions(excelQuestReward.conditions),
    };
  }
}
