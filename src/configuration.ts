import * as path from "path";

export const CONFIGURATION = {
  questsCsvPath: path.join(__dirname, '../', process.env.INPUT_QUEST_CVS_PATH || 'data/quest.csv'),
  questTargetsCsvPath: path.join(__dirname, '../', process.env.INPUT_QUEST_TARGETS_CSV_PATH || 'data/quest_targets.csv'),
  questRewardsCsvPath: path.join(__dirname, '../', process.env.INPUT_QUEST_REWARDS_CSV_PATH || 'data/quest_targets.json'),

  questResultJsonPath: path.join(__dirname, '../', process.env.OUTPUT_QUESTS_JSON_PATH || 'data/result.json'),

  csvParserEncoding: process.env.CSV_PARSER_ENCODING || 'utf-8',
  csvParserDelimiter: process.env.CSV_PARSER_DELIMITER || ',',
  csvParserQuote: process.env.CSV_PARSER_QUOTE || '"',
} as const;
