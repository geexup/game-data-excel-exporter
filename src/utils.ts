import * as fs from 'fs';
import { parse } from 'csv-parse/sync';
import { ExcelQuestData, ExcelQuestTarget, ExcelQuestReward, Nullable } from "./interfaces/index";
import { CONFIGURATION } from "./configuration";

export const readCsv = (path: string) => {
    const { csvParserQuote, csvParserEncoding, csvParserDelimiter } = CONFIGURATION;

    if (!fs.existsSync(path)) {
        console.error(`File not found: ${path}`);
        return [];
    }
    const fileContent = fs.readFileSync(path, {encoding: 'utf-8'});
    return parse(fileContent, {
        columns: true,
        encoding: csvParserEncoding as BufferEncoding,
        delimiter: csvParserDelimiter,
        quote: csvParserQuote,
    }) as Record<string, Nullable<string>>[];
};

export const readQuestsCsv = (path: string): ExcelQuestData[] => {
    const fileData = readCsv(path);
    return fileData.map(row => ({
        id: row['Id'] ? +row['Id'] : 0,
        name: row['Name'] ?? '',
        description: row['Description'] ?? '',
        cooldown: row['Cooldown'] ? +row['Cooldown'] : -1,
        timelimit: row['TimeLimit'] ? +row['TimeLimit'] : -1,
        requiredQuests: row['RequiredQuests']?.split(',')?.map(id => +id) ?? [],
    }));
};

export const readQuestTargetsCsv = (path: string): ExcelQuestTarget[] => {
    const fileData = readCsv(path);
    return fileData.map(row => ({
        quest_id: row['quest_id'] ? +row['quest_id'] : 0,
        description: row['description'] ?? '',
        questType: row['questType'] ? +row['questType'] : 0,
        type: row['type'] ?? '',
        count: row['count'] ? +row['count'] : 0,
        extra: row['extra'] ?? '',
        conditions: row['conditions'] ?? '',
    }));
};
export const readQuestRewardsCsv = (path: string): ExcelQuestReward[] => {
    const fileData = readCsv(path);
    return fileData.map(row => ({
        quest_id: row['quest_id'] ? +row['quest_id'] : 0,
        description: row['description'] ?? '',
        type: row['type'] ?? '',
        count: row['count'] ? +row['count'] : 0,
        extra: row['extra'] ?? '',
        conditions: row['conditions'] ?? '',
    }));
};
export const makeReverseLinkedMap = <T, TKey extends keyof T>(items: T[], idKey: TKey) => new Map<T[TKey], T[]>(
  items.reduce((acc, item) => {
    const id = item[idKey] as TKey;
    const items = acc.find(([key]) => key === id)?.[1] ?? [];
    if (items.length === 0) acc.push([id as T[TKey], items]);
    items.push(item);
    return acc;
  }, [] as Array<[T[TKey], T[]]>),
);

export const saveResultAsFile = <T = unknown>(result: T, dest: string) => {
    const stringified = JSON.stringify(result);
    fs.writeFileSync(dest, stringified, { encoding: 'utf-8' });
    console.info(`Result saved to ${dest}`);
};