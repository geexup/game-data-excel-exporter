import 'dotenv/config';

import { QuestMapper } from "./mappers";
import { saveResultAsFile, readQuestsCsv, readQuestTargetsCsv, readQuestRewardsCsv } from "./utils";
import { CONFIGURATION } from "./configuration";

async function main() {
  const { questsCsvPath, questTargetsCsvPath, questRewardsCsvPath, questResultJsonPath } = CONFIGURATION;

  const questFile = readQuestsCsv(questsCsvPath);
  const questTargetsFile = readQuestTargetsCsv(questTargetsCsvPath);
  const questRewardsFile = readQuestRewardsCsv(questRewardsCsvPath);

  const questMapper = new QuestMapper(questFile, questTargetsFile, questRewardsFile);
  const result = questMapper.mapAll();

  saveResultAsFile(result, questResultJsonPath);
}

void main();
