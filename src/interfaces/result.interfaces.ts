export type QuestCondition = [number, number];

export interface QuestTarget {
  /**
   * класснейм
   * @example "SurvivorM_Mirek"
   */
  type: string;
  /**
   * количество
   * @example 5
   */
  count: number;
  /**
   * строка, используется в заданиях на убийство, означает оружие с которого нужно убивать цели
   * @example "KitchenKnife"
   */
  extra: string;
  /**
   * целочисленное значение, определяет тип задания: убийство, поиск вещей и тд.
   * @example 1
   */
  questType: number;
  /**
   * строка, используется в графе Цели, в книжке с заданиями
   * @example "Выживший Мирек"
   */
  description: string;
  /**
   * @example [-1, -1]
   */
  conditions: QuestCondition;
}

export interface QuestReward {
  /**
   * класснейм
   * @example "WaterBottle"
   */
  type: string;
  /**
   * количество
   * @example 3
   */
  count: number;
  /** если строка отсутствует, либо она пустая, убийство засчитывается в любом случае */
  extra: string;
  /**
   * строка, используется в графе Цели, в книжке с заданиями
   * @example "Родникова вода"
   */
  description: string;
  /**
   * @example [-1, -1]
   */
  conditions: QuestCondition;
}

/**
 * Структура квеста
 */
export interface QuestResult {
  /* целочисленное значение, должно быть уникальным */
  ID: number;

  /**
   * интервал между выполнением задания в секундах,
   * -1 для единоразового выполнения
   */
	Cooldown: number;
  /**
   * ограничение времени на выполнение,
   * в секундах 3600 = 1 час
   * -1 для отключения
   */
	TimeLimit: number;
  /** Название задания */
	Name: string;
  /** Развернутое описание задания */
	Description: string;

  /**
   * массив ID заданий, которые дожны быть выполнены для взятия этого задания
   * @example "RequiredQuests" : [1, 3, 4] (Это задание можно взять если выполнены эти 3 предыдущих)
   */
  RequiredQuests: number[];
  /** цели задания */
	Targets: QuestTarget[];
  /** список наград */
	Rewards: QuestReward[]; //   (список наград)
}
