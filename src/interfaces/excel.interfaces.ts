export interface ExcelQuestData {
  id: number;
  name: string;
  description: string;
  cooldown: number;
  timelimit: number;
  requiredQuests: number[];
}

export interface ExcelQuestTarget {
  quest_id: number; // link to ExcelQuestData
  description: string;
  type: string;
  questType: number;
  extra?: string;
  count: number;
  conditions: string;
}

export interface ExcelQuestReward {
  quest_id: number; // link to ExcelQuestData
  description: string;
  type: string;
  count: number;
  extra?: string;
  conditions: string;
}
