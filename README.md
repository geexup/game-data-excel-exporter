# Quest Game / Excel Exporter

## Requirements
- node (https://nodejs.org/en/)

## Installation && Build

Install dependencies
```bash
npm install
```

Then compile the project
```bash
npm run compile
```

## Usage

```bash
npm start
```
